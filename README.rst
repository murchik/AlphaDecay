##########
AlphaDecay
##########

Since the version of 3.13 ProtonMail email composer makes it impossible
for owners of free accounts to remove the "Sent with ProtonMail Secure Email"
tagline from signature settings. Thus making it hard for you in a way not to
spam your peers with every email you send.

This Greasemonkey / Tampermonkey script automatically removes the tagline every
time you create a new email.
